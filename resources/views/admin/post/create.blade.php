@extends('layout')

@section('content')
    <div class="container">
        <h1>Create New Post</h1>
        <form action="{{ route('post.create') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
        
                <label for="body">What's up?</label>
                
                <text-editor names="body"> </text-editor> 
            <button class="btn btn-primary">Save</button>
        </form>
        
</div>
@endsection

