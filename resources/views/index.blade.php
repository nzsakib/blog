@extends('layout')

@section('content')
    <div class="container">
        @foreach($posts as $post)
            <a href="{{ route('post.show', $post->id) }}">
                <h1>{{ $post->title }}</h1>

            </a>
            <p>{!! $post->body !!}</p>
            <span class="published"> {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }} </span>

        @endforeach
    </div>
@endsection
