<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Nazmus Sakib | All Post</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.5/tinymce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.5/plugins/imagetools/plugin.min.js"></script>

    <script src="/js/app.js"></script>

<script>
    // tinymce.init({
    //     selector: 'textarea.form-control'
    // });
    
</script>
</body>
</html>
