<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class AdminController extends Controller
{
    public function getPostForm()
    {
        return view('admin.post.create');
    }
    public function createPost()
    {
        $data = request()->all();
        $data['status'] = 0;
        $data['user_id'] = 1;
        // dd($data);
        Post::create($data);
        return back();
    }
}
