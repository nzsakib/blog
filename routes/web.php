<?php

Route::get('/', 'PostController@index')->name('post.all');
Route::get('/post/{id}', 'PostController@show')->name('post.show');
Route::get('/admin/post/create', 'AdminController@getPostForm'); // todo: Lock down
Route::post('/admin/post/create', 'AdminController@createPost')->name('post.create');

